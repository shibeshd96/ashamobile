/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

//  import Login from './src/screens/login';

//  export default Login;

import {createStackNavigator, createAppContainer} from 'react-navigation';
import Login from './src/screens/login';
import MainApp from './MainApp'

const MainNavigator = createStackNavigator({
  Login: {screen: Login, navigationOptions: { header: null }},
  MainApp: {screen: MainApp, navigationOptions: { header: null }},
});

const App = createAppContainer(MainNavigator);

export default App;