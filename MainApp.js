import React from "react";
import { createAppContainer, createDrawerNavigator } from "react-navigation";

import CustomDrawerNavigator from "./src/navigations/CustomDrawer/CustomDrawerNavigator";
import Dashboard from "./src/screens/dashboard";
import GroupLeadershipInfo from "./src/screens/GroupLeadershipInfo";
import SubProjectInfo from "./src/screens/SubProjectInfo";
import SubProjectAchievement from "./src/screens/SubProjectAchievement";


const MainNavigator = createDrawerNavigator(
  {
    Dashboard: {
      navigationOptions: {
        drawerLabel: "Home"
      },
      screen: Dashboard
    },

    GroupLeadershipInfo: {
      navigationOptions: {
        drawerLabel: "Group Leadership Information"
      },
      screen: GroupLeadershipInfo
    },

    SubProjectInfo: {
      navigationOptions: {
        drawerLabel: "Sub Project Information"
      },
      screen: SubProjectInfo
    },

    SubProjectAchievement: {
        navigationOptions: {
          drawerLabel: "Sub Project Achievement"
        },
        screen: SubProjectAchievement
      }
  },
  {
    contentComponent: CustomDrawerNavigator
  }
);

const MainApp = createAppContainer(MainNavigator);
export default MainApp;