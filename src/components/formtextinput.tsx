import * as React from "react";
import { StyleSheet, Text, TextInput, TextInputProps, View, NativeSyntheticEvent, Platform, TextInputFocusEventData } from "react-native";
import colors from "../config/colors";

type Props = TextInputProps & {
  error?: string;
};

interface State {
  isFocused: boolean;
}

class FormTextInput extends React.Component<Props> {
  textInputRef = React.createRef<TextInput>();

  readonly state: State = {
    isFocused: false
  };

  focus = () => {
    if (this.textInputRef.current) {
      this.textInputRef.current.focus();
    }
  };

  handleFocus = (
    e: NativeSyntheticEvent<TextInputFocusEventData>
  ) => {
    this.setState({ isFocused: true });
      if (this.props.onFocus) {
      this.props.onFocus(e);
    }
  };

  handleBlur = (
    e: NativeSyntheticEvent<TextInputFocusEventData>
  ) => {
    this.setState({ isFocused: false });
    if (this.props.onBlur) {
      this.props.onBlur(e);
    }
  };

  render() {
    const { error, onFocus, onBlur, style, ...otherProps } = this.props;
    const { isFocused } = this.state;
    return (
      <View style={[styles.container, style]}>
        <TextInput
          ref={this.textInputRef}
          selectionColor={colors.DBLUE}
          underlineColorAndroid={
            isFocused
              ? colors.DBLUE
              : colors.GREY
          }
          style={styles.textInput}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          {...otherProps} />
          <Text style={styles.errorText}>{error || ""}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10
  },
  textInput: {
    height: 40,
    paddingLeft: 6,
    marginBottom: 10
  },
  errorText: {
    height: 20,
    color: colors.DRED,
    paddingLeft: 6
  }
});

export default FormTextInput;