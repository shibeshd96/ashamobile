const colors = {
    BLACK: '#000',
    GREY: '#858585',
    WHITE: '#fff',
    GREEN: '#7bf542',
    DRED: '#800000',
    DBLUE: '#1f7e87',
    DREDOP: '#80000033',
    DBLUEOP: '#1f7e875e'
};

export default colors;