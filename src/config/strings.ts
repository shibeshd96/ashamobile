const strings = {
    LOGIN: "Asha MIS Login",
    WELCOME_TO_APP: "Welcome to Asha MIS!",
    USERNAME_PLACEHOLDER: "Enter Username",
    PASSWORD_PLACEHOLDER: "Enter Password",
    USERNAME_REQUIRED: "This field cannot be empty!",
    PASSWORD_REQUIRED: "Password cannot be empty!",
    NAME_PLACEHOLDER: "Name",
    GROUPNAME_PLACEHOLDER: "Name of Group/Cooperative/Comittee",
    CASTE_PLACEHOLDER: "Caste",
    SUBMIT: "Submit",
    MENUICON: "bars",
    GETLOCATION: "Get Current Location",
    SAVELOCATION: "Save Location",
    UNIT_PLACEHOLDER: "Unit",
    PHYSICAL_TARGET_PLACEHOLDER: "Physical Target Achieved",
    FINANCIAL_TARGET_PLACEHOLDER: "Financial Target Achieved",
    CASH_PLACEHOLDER: "Cash",
    LOAN_PLACEHOLDER: "Loan",
    LABOR_PLACEHOLDER: "Labor",
    KIND_PLACEHOLDER: "Kind",
    OTHER_PLACEHOLDER: "Other",
    LOAD_DATA: "Load Data"
  };
  
  export default strings;