import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { DrawerItems } from "react-navigation";


const CustomDrawerNavigator = (props: JSX.IntrinsicAttributes & import("react-navigation").DrawerItemsProps & { children?: React.ReactNode; }) => (
  <View style={[styles.container]}>
    <Image source={require('../../assets/images/asha.png')} style={styles.logo} />
    <View style={styles.drawer}>
      <DrawerItems
        activeBackgroundColor={"black"}
        activeTintColor={"white"}
        iconContainerStyle={styles.icons}
        {...props}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
  
    icons: {
      width: 30
    },

    drawer: {
      flex: 2
    },
    
    logo: {
      flex: 1,
      width: "80%",
      height: "auto",
      resizeMode: "contain",
      alignSelf: "center"
    },
  });

export default CustomDrawerNavigator;