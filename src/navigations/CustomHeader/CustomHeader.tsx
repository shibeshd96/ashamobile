import React from "react";
import { View, StyleSheet, Image, TouchableOpacity} from "react-native";
import { DrawerItems } from 'react-navigation';

const CustomHeader = ({navigation} : {navigation: any}) => (
  <View style={[styles.container]}>
    <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.logocontainer}>
        <Image source={require('../../assets/images/menu.png')} style={styles.logo}  />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
    container: {
      paddingTop: 20,
      width: '100%',
      height: 60,
      flexDirection: "row",
      justifyContent: 'flex-start',
    },
    logo: {
        width: 30,
        height: 30,
    },
    logocontainer: {
        paddingLeft: 20,
    }
  });
  

export default CustomHeader;