import * as React from "react";
import { Dimensions, Image, StyleSheet, View, Text, KeyboardAvoidingView, Picker, TouchableOpacity, ScrollView } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';

import Button from "../components/button";
import FormTextInput from "../components/formtextinput";
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";
import CustomHeader from "../navigations/CustomHeader";


interface Props {
    navigation?: any;
  }

interface State {
    name: string;
    groupname: string;
    nameTouched: boolean;
    groupnameTouched: boolean;
    post: string;
    gender: string;
    caste: string;
    casteTouched: boolean;
    isDateTimePickerVisible: boolean;
    date: string;
    vulnerability: string;
    ethnicity: string;
  }


class GroupLeadershipInfo extends React.Component<Props,State> {
    groupnameInputRef = React.createRef<FormTextInput>();
    casteInputRef = React.createRef<FormTextInput>();


    public state: State = {
        name: "",
        groupname: "",
        nameTouched: false,
        groupnameTouched: false,
        post: "",
        gender: "",
        caste: "",
        casteTouched: false,
        isDateTimePickerVisible: false,
        date: 'Select Date -->',
        vulnerability: '',
        ethnicity: ''
      };

      handleNameChange = async (name: string) => {
        await this.setState({ name: name });
      };

      handleNameSubmitPress = () => {
        if (this.groupnameInputRef.current) {
          this.groupnameInputRef.current.focus();
        }
      };

      handleNameBlur = () => {
        this.setState({ nameTouched: true });
      };

      handleGroupNameChange = async (groupname: string) => {
        await this.setState({ groupname: groupname });
      };

      handleGroupNameSubmitPress = () => {
        if (this.casteInputRef.current) {
          this.casteInputRef.current.focus();
        }
      };

      handleGroupNameBlur = () => {
        this.setState({ groupnameTouched: true });
      };

      handleCasteChange = async (caste: string) => {
        await this.setState({ caste: caste });
      };

      handleCasteSubmitPress = () => {
        if (this.groupnameInputRef.current) {
          this.groupnameInputRef.current.focus();
        }
      };

      handleCasteBlur = () => {
        this.setState({ casteTouched: true });
      };

      showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
      };
    
      hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
      };
    
      handlePicker = async (date: Date) => {
        this.setState({
            date: moment(date).format('MM-DD-YYYY'),
            isDateTimePickerVisible: false
        })
      }


    render() {
        const {
            name,
            groupname,
            nameTouched,
            groupnameTouched,
            post,
            gender,
            caste,
            casteTouched,
          } = this.state;

        const nameError =
            !name && nameTouched
            ? strings.USERNAME_REQUIRED
            : undefined;

        const groupnameError =
            !groupname && groupnameTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const casteError =
            !caste && casteTouched
            ? strings.USERNAME_REQUIRED
            : undefined;

        return (
          <ScrollView>
            <CustomHeader navigation={this.props.navigation} />
            <KeyboardAvoidingView
              style={styles.container}
              behavior={constants.IS_IOS ? "padding" : undefined}
            >
              <View style={styles.form}>
                <FormTextInput
                  value={this.state.name}
                  onChangeText={this.handleNameChange}
                  placeholder={strings.NAME_PLACEHOLDER}
                  onSubmitEditing={this.handleNameSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleNameBlur}
                  error={nameError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  ref={this.groupnameInputRef}
                  value={this.state.groupname}
                  onChangeText={this.handleGroupNameChange}
                  placeholder={strings.GROUPNAME_PLACEHOLDER}
                  onSubmitEditing={this.handleGroupNameSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleGroupNameBlur}
                  error={groupnameError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <Picker
                  style={styles.Picker}
                  selectedValue={this.state.post}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ post: itemValue })
                  }
                >
                  <Picker.Item label="--Select Post--" value="" />
                  <Picker.Item label="Option 1" value="1" />
                  <Picker.Item label="Option 2" value="2" />
                  <Picker.Item label="Option 3" value="3" />
                  <Picker.Item label="Option 4" value="4" />
                </Picker>

                <Picker
                  style={styles.Picker}
                  selectedValue={this.state.gender}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ gender: itemValue })
                  }
                >
                  <Picker.Item label="--Select Gender--" value="" />
                  <Picker.Item label="Male" value="Male" />
                  <Picker.Item label="Female" value="Female" />
                  <Picker.Item label="Other" value="Other" />
                </Picker>

                <FormTextInput
                  ref={this.casteInputRef}
                  value={this.state.caste}
                  onChangeText={this.handleCasteChange}
                  placeholder={strings.CASTE_PLACEHOLDER}
                  onSubmitEditing={this.handleGroupNameSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleCasteBlur}
                  error={casteError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <View style={styles.date}>
                  <Text style={styles.pickedDate}>{this.state.date}</Text>

                  {/* <Button label="datetime" onPress={this.showDateTimePicker} /> */}
                  <TouchableOpacity onPress={this.showDateTimePicker}>
                    <Image
                      source={require("../assets/images/date.png")}
                      style={styles.datebutton}
                    />
                  </TouchableOpacity>

                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hideDateTimePicker}
                    mode={"date"}
                  />
                </View>

                <Picker
                  style={styles.Picker}
                  selectedValue={this.state.vulnerability}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ vulnerability: itemValue })
                  }
                >
                  <Picker.Item label="--Select Vulnerability--" value="" />
                  <Picker.Item label="Option 1" value="1" />
                  <Picker.Item label="Option 2" value="2" />
                  <Picker.Item label="Option 3" value="3" />
                  <Picker.Item label="Option 4" value="4" />
                </Picker>

                <Picker
                  style={styles.Picker}
                  selectedValue={this.state.ethnicity}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ ethnicity: itemValue })
                  }
                >
                  <Picker.Item label="--Select Ethnicity--" value="" />
                  <Picker.Item label="Option 1" value="1" />
                  <Picker.Item label="Option 2" value="2" />
                  <Picker.Item label="Option 3" value="3" />
                </Picker>

                <Button
                  label={strings.SUBMIT}
                  onPress={() => alert("pressed")}
                />
              </View>
            </KeyboardAvoidingView>
          </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.WHITE,
      alignItems: "center",
      justifyContent: "space-between"
    },
    logo: {
      flex: 1,
      width: "80%",
      height: "auto",
      resizeMode: "contain",
      alignSelf: "center"
    },
    form: {
      flex: 1,
      justifyContent: "center",
      width: "80%"
    },
    heading: {
        fontSize: 24,
        color: colors.DRED,
        fontWeight: 'bold'
    },
    Picker: {
        height: 50,
        width: "100%",
        marginBottom: 35
    },
    date: {
        flexDirection: "row",
        width: '100%',
        marginBottom: 40
    },
    datebutton: {
        flex: 1,
        width: 30,
        height: 30
    },
    pickedDate: {
        flex: 2,
        padding: 10,
        borderWidth: 0.5,
        marginRight: 15,
        backgroundColor: "#f7f7f7"
    }
  });

export default GroupLeadershipInfo;