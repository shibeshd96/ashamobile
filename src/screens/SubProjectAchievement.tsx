import * as React from "react";
import { Dimensions, Image, StyleSheet, View, Text, KeyboardAvoidingView, Picker, TouchableOpacity, ScrollView } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';

import Button from "../components/button";
import FormTextInput from "../components/formtextinput";
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";
import CustomHeader from "../navigations/CustomHeader";

interface Props {
    navigation?: any;
  }

interface State {
    unit: string;
    unitTouched: boolean;
    physicalTarget: string;
    physicalTargetTouched: boolean;
    financialTarget: string;
    financialTargetTouched: boolean;
    cash: string;
    cashTouched: boolean;
    loan: string;
    loanTouched: boolean;
    labor: string;
    laborTouched: boolean;
    kind: string;
    kindTouched: boolean;
    other: string;
    otherTouched: boolean;
    date: string;
    isDateTimePickerVisible: boolean;
  }


class SubProjetAchievement extends React.Component<Props,State> {
    physicalTargetRef = React.createRef<FormTextInput>();
    financialTargetRef = React.createRef<FormTextInput>();
    cashRef = React.createRef<FormTextInput>();
    loanRef = React.createRef<FormTextInput>();
    laborRef = React.createRef<FormTextInput>();
    kindRef = React.createRef<FormTextInput>();
    otherRef = React.createRef<FormTextInput>();

    public state: State = {
        unit: "",
        unitTouched: false,
        physicalTarget: "",
        physicalTargetTouched: false,
        financialTarget: "",
        financialTargetTouched: false,
        cash: "",
        cashTouched: false,
        loan: "",
        loanTouched: false,
        labor: "",
        laborTouched: false,
        kind: "",
        kindTouched: false,
        other: "",
        otherTouched: false,
        date: "Select Date -->",
        isDateTimePickerVisible: false,
      };

      //Event Handlers for Unit

      handleUnitChange = async (unit: string) => {
        await this.setState({ unit: unit });
      };

      handleUnitSubmitPress = () => {
        if (this.physicalTargetRef.current) {
          this.physicalTargetRef.current.focus();
        }
      };

      handleUnitBlur = () => {
        this.setState({ unitTouched: true });
      };

      //End Event Handlers for Unit

      //Event Handlers for Physical Target

      handlePhysicalTargetChange = async (physicalTarget: string) => {
        await this.setState({ physicalTarget: physicalTarget });
      };

      handlePhysicalTargetSubmitPress = () => {
        if (this.financialTargetRef.current) {
          this.financialTargetRef.current.focus();
        }
      };

      handlePhysicalTargetBlur = () => {
        this.setState({ physicalTargetTouched: true });
      };

      //End Event Handlers for Physical Target

      //Event Handlers for Date Picker
     
      showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
      };
    
      hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
      };
    
      handlePicker = async (date: Date) => {
        this.setState({
            date: moment(date).format('MM-DD-YYYY'),
            isDateTimePickerVisible: false
        })
      }

      //End Event Handlers for Date Picker

      //Event Handlers for Financial Target

      handleFinancialTargetChange = async (financialTarget: string) => {
        await this.setState({ financialTarget: financialTarget });
      };

      handleFinancialTargetSubmitPress = () => {
        if (this.cashRef.current) {
          this.cashRef.current.focus();
        }
      };

      handleFinancialTargetBlur = () => {
        this.setState({ financialTargetTouched: true });
      };

      //End Event Handlers for Financial Target

      //Event Handlers for Cash

      handleCashChange = async (cash: string) => {
        await this.setState({ cash: cash });
      };

      handleCashSubmitPress = () => {
        if (this.loanRef.current) {
          this.loanRef.current.focus();
        }
      };

      handleCashBlur = () => {
        this.setState({ cashTouched: true });
      };

      //End Event Handler for Cash

      //Event Handlers for Loan

      handleLoanChange = async (loan: string) => {
        await this.setState({ loan: loan });
      };

      handleLoanSubmitPress = () => {
        if (this.laborRef.current) {
          this.laborRef.current.focus();
        }
      };

      handleLoanBlur = () => {
        this.setState({ loanTouched: true });
      };

      //End Event Handler for Loan

      //Event Handlers for Labor

      handleLaborChange = async (labor: string) => {
        await this.setState({ labor: labor });
      };

      handleLaborSubmitPress = () => {
        if (this.kindRef.current) {
          this.kindRef.current.focus();
        }
      };

      handleLaborBlur = () => {
        this.setState({ laborTouched: true });
      };

      //End Event Handler for Labor

      //Event Handlers for Kind

      handleKindChange = async (kind: string) => {
        await this.setState({ kind: kind });
      };

      handleKindSubmitPress = () => {
        if (this.otherRef.current) {
          this.otherRef.current.focus();
        }
      };

      handleKindBlur = () => {
        this.setState({ kindTouched: true });
      };

      //End Event Handler for Kind

      //Event Handlers for Other
      handleOtherChange = async (other: string) => {
        await this.setState({ other: other });
      };

      handleOtherBlur = () => {
        this.setState({ otherTouched: true });
      };

      //End Event Handler for Other


    render() {
        const {
            unit,
            unitTouched,
            physicalTarget,
            physicalTargetTouched,
            financialTarget,
            financialTargetTouched,
            cash,
            cashTouched,
            loan,
            loanTouched,
            labor,
            laborTouched,
            kind,
            kindTouched,
            other,
            otherTouched,
          } = this.state;

        const unitError =
            !unit && unitTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const physicalTargetError =
            !physicalTarget && physicalTargetTouched
            ? strings.USERNAME_REQUIRED
            : undefined;

        const financialTargetError =
            !financialTarget && financialTargetTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const cashError =
            !cash && cashTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const loanError =
            !loan && loanTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const laborError =
            !labor && laborTouched
            ? strings.USERNAME_REQUIRED
            : undefined;

        const kindError =
            !kind && kindTouched
            ? strings.USERNAME_REQUIRED
            : undefined;
        
        const otherError =
            !other && otherTouched
            ? strings.USERNAME_REQUIRED
            : undefined;


        return (
          <ScrollView>
            <CustomHeader navigation={this.props.navigation} />
            <KeyboardAvoidingView
              style={styles.container}
              behavior={constants.IS_IOS ? "padding" : undefined}
            >
              <View style={styles.form}>
                <FormTextInput
                  value={this.state.unit}
                  onChangeText={this.handleUnitChange}
                  placeholder={strings.UNIT_PLACEHOLDER}
                  onSubmitEditing={this.handleUnitSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleUnitBlur}
                  error={unitError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.physicalTarget}
                  onChangeText={this.handlePhysicalTargetChange}
                  placeholder={strings.PHYSICAL_TARGET_PLACEHOLDER}
                  onSubmitEditing={this.handlePhysicalTargetSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handlePhysicalTargetBlur}
                  error={physicalTargetError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.financialTarget}
                  onChangeText={this.handleFinancialTargetChange}
                  placeholder={strings.FINANCIAL_TARGET_PLACEHOLDER}
                  onSubmitEditing={this.handleFinancialTargetSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleFinancialTargetBlur}
                  error={financialTargetError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.cash}
                  onChangeText={this.handleCashChange}
                  placeholder={strings.CASH_PLACEHOLDER}
                  onSubmitEditing={this.handleCashSubmitPress}
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onBlur={this.handleCashBlur}
                  error={cashError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.loan}
                  onChangeText={this.handleLoanChange}
                  placeholder={strings.LOAN_PLACEHOLDER}
                  onSubmitEditing={this.handleLoanSubmitPress}
                  autoCorrect={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  onBlur={this.handleLoanBlur}
                  error={loanError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.labor}
                  onChangeText={this.handleLaborChange}
                  placeholder={strings.LABOR_PLACEHOLDER}
                  onSubmitEditing={this.handleLaborSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleLaborBlur}
                  error={laborError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.kind}
                  onChangeText={this.handleKindChange}
                  placeholder={strings.KIND_PLACEHOLDER}
                  onSubmitEditing={this.handleKindSubmitPress}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleKindBlur}
                  error={kindError}
                  blurOnSubmit={constants.IS_IOS}
                />

                <FormTextInput
                  value={this.state.other}
                  onChangeText={this.handleOtherChange}
                  placeholder={strings.OTHER_PLACEHOLDER}
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  onBlur={this.handleOtherBlur}
                  error={otherError}
                  blurOnSubmit={constants.IS_IOS}
                />

                

                <View style={styles.date}>
                  <Text style={styles.pickedDate}>{this.state.date}</Text>
                  <TouchableOpacity onPress={this.showDateTimePicker}>
                    <Image
                      source={require("../assets/images/date.png")}
                      style={styles.datebutton}
                    />
                  </TouchableOpacity>

                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hideDateTimePicker}
                    mode={"date"}
                  />
                </View>


                <Button
                  label={strings.SUBMIT}
                  onPress={() => alert("pressed")}
                />
              </View>
            </KeyboardAvoidingView>
          </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.WHITE,
      alignItems: "center",
      justifyContent: "space-between"
    },
    logo: {
      flex: 1,
      width: "80%",
      height: "auto",
      resizeMode: "contain",
      alignSelf: "center"
    },
    form: {
      flex: 1,
      justifyContent: "center",
      width: "80%"
    },
    heading: {
        fontSize: 24,
        color: colors.DRED,
        fontWeight: 'bold'
    },
    Picker: {
        height: 50,
        width: "100%",
        marginBottom: 35
    },
    date: {
        flexDirection: "row",
        width: '100%',
        marginBottom: 40
    },
    datebutton: {
        flex: 1,
        width: 30,
        height: 30
    },
    pickedDate: {
        flex: 2,
        padding: 10,
        borderWidth: 0.5,
        marginRight: 15,
        backgroundColor: "#f7f7f7"
    }
  });

export default SubProjetAchievement;