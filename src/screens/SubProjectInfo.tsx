import * as React from "react";
import { PermissionsAndroid, Image, StyleSheet, View, Text, KeyboardAvoidingView, TouchableOpacity, ImageBackground, Picker } from "react-native";
import Geolocation from '@react-native-community/geolocation';
import Button from "../components/button";
import FormTextInput from "../components/formtextinput";
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";

import CustomHeader from "../navigations/CustomHeader";

interface Props {
    navigation?: any;
  }

interface State {
  location: string;
  currentLongitude: string;
  currentLatitude: string;
  household: string;
}



class SubProjectInfo extends React.Component<Props, State> {
  public state: State = {
    location: 'Latitude: NA | Longitude: NA',
    currentLatitude: 'NA',
    currentLongitude: 'NA',
    household: ''
  }

  getLocation = () => {
    var that = this;

    if(constants.IS_IOS === true) {
      this.callLocation(that);
    }else {
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
              'title': 'Location Access Required',
              'message': 'This App needs to Access your location',
              'buttonPositive': 'Ok'
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            that.callLocation(that);
          } else {
            alert("Permission Denied");
          }
        } catch (err) {
          alert(err);
          console.warn(err)
        }
  
      } requestLocationPermission();
    }
  }

  callLocation = (that: { setState: { (arg0: { currentLongitude: string; }): void; (arg0: { currentLatitude: string; }): void; }; }) => {
    Geolocation.getCurrentPosition(
      //Will give you the current location
       (position) => {
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);
          //getting the Latitude from the location json
          that.setState({ currentLongitude:currentLongitude });
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({ currentLatitude:currentLatitude });
          //Setting state Latitude to re re-render the Longitude Text
       },
       (error) => alert(error.message),
       { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

    render() {
        return(
            <KeyboardAvoidingView style={styles.container} behavior={constants.IS_IOS ? "padding" : undefined}>
                <CustomHeader navigation={this.props.navigation} />

                <Image source={require('../assets/images/worldwide-location.png')} style={styles.logo} />
                
                <View style={styles.locationView}>
                  <Picker
                    style={styles.Picker}
                    selectedValue={this.state.household}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ household: itemValue })
                    }>
                    <Picker.Item label="--Select Household--" value="" />
                    <Picker.Item label="Option 1" value="1" />
                    <Picker.Item label="Option 2" value="2" />
                    <Picker.Item label="Option 3" value="3" />
                    <Picker.Item label="Option 4" value="4" />
                  </Picker>

                  <Text style={styles.location}>Latitude: {this.state.currentLatitude} | Longitude: {this.state.currentLongitude}</Text>

                  <TouchableOpacity style={styles.touchable1} onPress = {this.getLocation}>
                    <Text style={{color: colors.WHITE, textAlignVertical: 'center'}}>{strings.GETLOCATION}</Text>
                    <Image
                      source={require("../assets/images/location_white.png")}
                      style={styles.locationbutton}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.touchable2}>
                    <Text style={{color: colors.WHITE, textAlignVertical: 'center'}}>{strings.SAVELOCATION}</Text>
                    <Image
                      source={require("../assets/images/save_white.png")}
                      style={styles.locationbutton}
                    />
                  </TouchableOpacity>
                </View>
                
            </KeyboardAvoidingView>
        );
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: "center",
    justifyContent: "space-between"
  },

  heading: {
    fontSize: 24,
    color: colors.DRED,
    fontWeight: "bold"
  },
  logo: {
    flex: 1,
    width: "60%",
    height: "auto",
    resizeMode: "contain",
    alignSelf: "center"
  },

  locationView: {
    flex: 2,
    width: "100%",
    flexDirection: "column",
    justifyContent: 'center',
  },
  touchable1: {
    flexDirection: 'row',
    justifyContent: "center",
    width: "60%",
    alignSelf: 'center',
    marginBottom: 10,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.DREDOP,
    backgroundColor: colors.DRED,
    padding: 8,

  },
  touchable2: {
    flexDirection: 'row',
    justifyContent: "center",
    width: "60%",
    alignSelf: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.DBLUEOP,
    backgroundColor: colors.DBLUE,
    padding: 8,

  },
  locationbutton: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  location: {
    padding: 10,
    borderWidth: 0.5,
    backgroundColor: "#f7f7f7",
    textAlign: "center",
    marginBottom: 20
  },
  Picker: {
    height: 50,
    width: "80%",
    alignSelf: 'center',
    marginBottom: 20
  },
});

export default SubProjectInfo;