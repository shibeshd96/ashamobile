import * as React from "react";
import { StyleSheet, Text, KeyboardAvoidingView, Picker, View } from "react-native";
import Button from "../components/button";
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";

import CustomHeader from "../navigations/CustomHeader";

interface Props {
    navigation?: any;
  }

interface State {
    fiscalyear: string;
}

class Dashboard extends React.Component<Props> {

    public state: State = {
        fiscalyear: ''
    }

    render() {
        return(
            <KeyboardAvoidingView style={styles.container} behavior={constants.IS_IOS ? "padding" : undefined}>
                <CustomHeader navigation={this.props.navigation} />
                <View style={styles.form}>
                    <Picker
                        style={styles.Picker}
                        selectedValue={this.state.fiscalyear}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({ post: itemValue })
                        }
                        >
                        <Picker.Item label="--Select Fiscal Year--" value="" />
                        <Picker.Item label="Option 1" value="1" />
                        <Picker.Item label="Option 2" value="2" />
                        <Picker.Item label="Option 3" value="3" />
                        <Picker.Item label="Option 4" value="4" />
                    </Picker>
                    <Button label={strings.LOAD_DATA} onPress={() => alert("Pressed")}/>
                </View>
                
            </KeyboardAvoidingView>
        );
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.WHITE,
      alignItems: "center",
      justifyContent: "space-between"
    },

    heading: {
      fontSize: 24,
      color: colors.DRED,
      fontWeight: 'bold'
    },

    Picker: {
        height: 50,
        width: "100%",
        marginBottom: 35
    },
    form: {
        flex: 1,
        justifyContent: "center",
        width: "80%"
      },
});

export default Dashboard;