import * as React from "react";
import { Image, StyleSheet, View, Text, KeyboardAvoidingView } from "react-native";
import Button from "../components/button";
import FormTextInput from "../components/formtextinput";
import colors from "../config/colors";
import strings from "../config/strings";
import constants from "../config/constants";

interface Props {
  navigation?: any;
}

interface State {
  username: string;
  password: string;
  userTouched: boolean;
  passwordTouched: boolean;
  responsedata: []
}

class Login extends React.Component<Props,State> {
  passwordInputRef = React.createRef<FormTextInput>();

  public state: State = {
    username: "",
    password: "",
    userTouched: false,
    passwordTouched: false,
    responsedata: []
  };

  handleUsernameChange = async (username: string) => {
    await this.setState({ username: username });
  };

  handlePasswordChange = async (password: string) => {
    await this.setState({ password: password });
  };

  handleUserSubmitPress = () => {
    if (this.passwordInputRef.current) {
      this.passwordInputRef.current.focus();
    }
  };

   handleUserBlur = () => {
    this.setState({ userTouched: true });
  };

  handlePasswordBlur = () => {
    this.setState({ passwordTouched: true });
  };


  handleLoginPress = () => {
    fetch('http://202.45.146.18/ashamis/mapi/verifylogin.php', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        txtname: this.state.username,
        txtpwd: this.state.password
      })
    }).then((response) => {
      console.log(response);
      // this.setState({ responsedata: response.body})

      // console.log(this.state.responsedata);
    }).catch((error) => {
      alert(error.message);
      throw error;
    })
  };

  render() {
    const {navigate} = this.props.navigation;
    const {
      username,
      password,
      userTouched,
      passwordTouched
    } = this.state;

    const userError =
    !username && userTouched
      ? strings.USERNAME_REQUIRED
      : undefined;
  const passwordError =
    !password && passwordTouched
      ? strings.PASSWORD_REQUIRED
      : undefined;

    return (
      <KeyboardAvoidingView style={styles.container} behavior={constants.IS_IOS ? "padding" : undefined}>
        <Image source={require('../assets/images/ashaMIS.png')} style={styles.logo} />
        {/* <Text style={styles.heading}>{strings.LOGIN}</Text> */}
        <View style={styles.form}>
          <FormTextInput
            value={this.state.username}
            onChangeText={this.handleUsernameChange}
            placeholder={strings.USERNAME_PLACEHOLDER}
            onSubmitEditing={this.handleUserSubmitPress}
            autoCorrect={false}
            keyboardType="email-address"
            returnKeyType="next"
            onBlur={this.handleUserBlur}
            error={userError}
            blurOnSubmit={constants.IS_IOS}
          />
          <FormTextInput
            ref={this.passwordInputRef}
            value={this.state.password}
            onChangeText={this.handlePasswordChange}
            placeholder={strings.PASSWORD_PLACEHOLDER}
            secureTextEntry={true}
            returnKeyType="done"
            onBlur={this.handlePasswordBlur}
            error={passwordError}
          />
          <Button label={strings.LOGIN} onPress={() => navigate('Dashboard')} disabled={!username || !password} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    alignItems: "center",
    justifyContent: "space-between"
  },
  logo: {
    flex: 1,
    width: "80%",
    height: "auto",
    resizeMode: "contain",
    alignSelf: "center"
  },
  form: {
    flex: 1,
    justifyContent: "center",
    width: "80%"
  },
  // heading: {
  //     fontSize: 24,
  //     color: colors.DRED,
  //     fontWeight: 'bold'
  // }
});

export default Login;
